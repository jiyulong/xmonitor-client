/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.snail.beans;

/**
 *
 * @author pm
 */
public class RequestResult {

    private boolean timeout, codeError, contentError,hasError;

    public boolean isTimeout() {
        return timeout;
    }

    public RequestResult setTimeout(boolean timeout) {
        this.timeout = timeout;
        return  this;
    }

    public boolean isCodeError() {
        return codeError;
    }

    public RequestResult setCodeError(boolean codeError) {
        this.codeError = codeError;
        return  this;
    }

    public boolean isContentError() {
        return contentError;
    }

    public RequestResult setContentError(boolean contentError) {
        this.contentError = contentError;
        return  this;
    }

    public boolean isHasError() {
        return hasError;
    }

    public RequestResult setHasError(boolean hasError) {
        this.hasError = hasError;
        return  this;
    }
 
}
